<?php


namespace Ipol\Fish\Core\Order;


use Ipol\Fish\Core\Entity\Collection;

/**
 * Class ItemCollection
 * @package Ipol\Fish\Core
 * @subpackage Order
 * @method false|Item getFirst
 * @method false|Item getNext
 * @method false|Item getLast
 */
class ItemCollection extends Collection
{
    /**
     * @var array
     */
    protected $items;

    /**
     * ItemCollection constructor.
     */
    public function __construct()
    {
        parent::__construct('items');
    }

}