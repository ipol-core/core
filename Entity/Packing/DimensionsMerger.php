<?php


namespace Ipol\Fish\Core\Entity\Packing;


/**
 * Interface DimensionsMerger
 * @package Ipol\Fish\Core
 * @subpackage Packing
 */
interface DimensionsMerger
{
    public static function getSumDimensions($arGabs);

}